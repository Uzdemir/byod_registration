'use strict';
const express = require('express');
const router = express.Router();
const getByodEventJSON = require('./../../handlers/api/getByodEventJSON.handler');
const sendUserWaiver = require('./../../handlers/api/sendUserWaiver.handler');

router.get('/getByodEventJSON', getByodEventJSON);
router.post('/sendUserWaiver', sendUserWaiver);

module.exports = router;