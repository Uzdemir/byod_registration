'use strict';
const express = require('express');
const router = express.Router();
const index = require('./index.rout');
const api = require('./api/api.rout');

router.use('/', index);
router.use('/api', api);

module.exports = router;
