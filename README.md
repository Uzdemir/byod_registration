# BYOD Registration #

#api (en)
    1. GET /api/getByodEventJSON?event_code=`code` - getting event_json file
    2. POST /api/sendUserWaiver - sending user waiver to spock

#api (ru)
    1. GET /api/getByodEventJSON?event_ode=`code` - метод, который отдает информацию о событии
    в формате JSON, если запрос приходит в первый раз, то устанавливается cookie `byod_registration_token`, как
    идентификатор сессии, приходит json, с общей информацией о событии, после выбора языка, нужно установить cookie
    `reg_la`, например reg_la=en или reg_la=es, когда установленна эта кука, в ответе прийдет registration_applications,
    где будут высланны необходимые поля для регистрации для конкретного языка, установленного в куках. Время жизни куки установленно на час.

    2. POST /api/sendUserWaiver - метод, который сохраняет данные о пользователе на сервере, изображение присылается в формате base64.
    Вот пример waiwer list
      "waiverList":{
    	"event": {
    		"eventId": "7578",
    		"uniqueEventCode":"BBPT1",
        	"stationId": "732335",
        	"waiver_creation_date": 1493203682920
    	},
    	"media": {
        	"file_count": 1,
        	"files":[
        		{"name":"test3.jpg", "base64":"/9j/4AAQSkZJRgABAQAAAQABAAD//gA+Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NjIpLCBkZWZhdWx0IHF1YWxpdHkK/9sAQwAIBgYHBgUIBwcHCQkICgwUDQwLCwwZEhMPFB0aHx4dGhwcICQuJyAiLCMcHCg3KSwwMTQ0NB8nOT04MjwuMzQy/9sAQwEJCQkMCwwYDQ0YMiEcITIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIy/8AAEQgAgACAAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A984xUNzdQ2kRmmkCIvUmnySLFEzyHCryTXEXFxJq920zk+Sp+Ue1bUqXO/I5sRiFSiak3id2ci0t9y9mOeaaNf1Ij/URfnWVLdxwArEAT7VWN3MxzvK/Su1YePY8qWLqdZG9/b2p/wDPCL86P7d1L/nhF+dYH2qbtI34UC5mzgyNn61X1eP8pKxc39pm/wD27qf/ADwi/Oj+3NT/AOeEf51g/apv+ep/Gj7VN/z1NH1dfyi+tz7s3v7c1P8A54R/nR/bmp/88I/zrB+1Tf8APU0fapv+eh/Oj6uv5R/W592b39u6n/zwj/Oj+3dT/wCeEf51g/apv+ehpPtU3/PU0fV4/wAofW5/zM3jr2pAf6mP/voVJF4nkRgt3bFR6rXOi8mB+/n61bivEnGyRcdsetTLDx/lHDFzv8R2ltdQ3kIkicOp7j+tWa4S3uZNJvFkRiYG+8ua7iKVZo1kQ5VhkEVxVaTg/I9bDYj2qs90Y/iWbydJZQSN5C1zZf7NYLjhiP1rd8W/8g9f98Vzt8f3EQrrwq91ep5+Pf7x+hSzRnHem1d0q1+2ajDCRlSct9BXVJqEWzzIpzkoLqaGlaDJfASzExw+ndq6KLQNOjXHkBvduavgLBD8owqjgVCWd+d4UV5kqs5u6Z70MPTpRSauzPuvDdlKh8kGJ+xXpXK31jPYT+XKPoexFd4srKwVyCp6EVT1uxW7098jLoNymro1pRl72plicLCdNygrM4bNFJ9ev1oz/nNeitjxRaKTNITTsIUnikBwc5opDyKLIE7bGiG+0aeSeWVf1rp/DU5l0lATnb8tcpZH9zKO2c10fhP/AJBzD/aNcWK+D5nqYCX7xeaYni0/8S5T/tCubvv9TDW744kaLSAy9dwrl7++2wwb069xVYVe6vUzzGaVR+gwcVt+F+dYH/XM1zyXUL4w/wCddB4UZW1kYIP7s1tWX7tnJg5L28bHZXOQg9M1X389Knuz+6H1qkCa82EdD3KztMm3Z4HrxVx4zLCyHHzAj9Kzc4IPpVxbsED5TROL6DpTVnc51vCM5YkXEYBPHBpP+ESuB/y8R/ka6T7V/sH86Ddf7B/OtPb1TN4XDu7OXk8KXarlJYmOOhJH9Kxbm1uLSTy7iMo3bPQ/jXoS3aFsMCPc9Kjv7CG/tmikHXoe4q4YqafvGVTAUpxvS3POqTNSXkTWd1JBKRvQ4J9R2qo1zEp5fn0FehF8yujxJe6+V7mrYn91Liuj8J/8g5v94/zrkbG8DW82xPxNdR4JkeXRyz4zvP8AOuTFq1P5npZdJOqvRjPHXOj/APA1rj9TH+jwV2Pjn/kD/wDA1rkNRH+jwVWC+Feplmn8V+hlge9dJ4KGNfH/AFzaueArpPBfGvD/AK5tXRiP4cjgwX8ePqd3fnbAD71neZzV7Uzi2H+9WTu/lXm0VeB72LlaoWPNNL5vpVbfmniKUjIjb8q1aSOaLk9ibzaTzT+FR+XN/wA82/Kjypv+eTflS90r377EnmZ69K0rFy9uN3rgVmR208jbQhUdya1Y0W3twCeg61jVaeiOvCqUZNy0R5/40jCa7kH70Skj35H9K5srzWxr16L/AFieVeUBCKfYd/zzWXjmvVoq1JHzmLkpVZWL2mD/AEab6f0rsfA3GjH/AHz/ADrkNNH+jTf57V2Hgf8A5A5/3z/OuXG/A15npZX/ABI+jG+OB/xKP+BrXJaiP3EFdf43/wCQSP8AeFclqI/cQU8H8K9RZp/FfoZoFdF4MH/E+H/XNq58Diui8HDGuj/rma6K/wDCkzgwX+8Q9TstXOLRf97+lYu/mtjWjtsgf9oVg7+TXFhleB6+OlasTo/7xfqK6hANg+lcgjgSKScciumW+twgHmr0FZ4iLdrGuBnFX5mWse1LxVb7fbf89V/Oj7fa5z5q/nXNyS7Ho+0p9xbu7hs4jJMxVfXaT/KuL1vxQ94jW9mhSPoXJ5P4dq7VbiCfKq6tntWHrnhuC6iaa2URzgZwOjfWtqDjGS50ceMVWpT/AHT0PPyPTtTCKnkRkYqwwynBHoe4qMivZuuXQ+VafXcvaaP9Hm+n9K67wT/yBz/vmuS04fuJvp/Sut8Ff8gg/wC+f51w4z4H6nsZX/EXoxPG4/4lI/3hXKaj/qIK6vxp82kj/eFcrqH+ohownwL1DM/4svQzwOK2PDU4ttcgZjhWyh/EcfriskCpY2aKRZFOGQhgfcV2TipRaPLoz9nOM/M9N1OIzafIFGWAyK5Td83vium0jUE1KyRwRvAww9DWZqmkSRu01uu5CeVHUV5uHn7OThI97G0/bQVWGpmbqN9QklSQeGHY0m8V2qNzyeZljd9PypN/0/Kod4pN1HKHPbqWBKyHcpKkdxXU6bcm7tFZ+WU7W+tcepLHGMnsB3rrtItmtLFRIPnY7mrlxaikrbnp5ZKbk+qOL8UWq2+tyFRgSKGwPy/pWIRW74nuBcazJg5EahRWKRXZQu6aueTire2k47FvTx+4m+ldZ4LGNJP++f51yun/AOomrqvBf/IKb/fP865sZrB+p35Z/Fj6MTxmM6SP94VzN4gksUcc7QDXaeILQ3WlyLg5AzXF6fMDGbaTqOOe/tUYWa5bG2ZU26ifdGeMU8DnrgetTz2TwElRlP5VCPSvRTPDs07F3TtQn06482E9eGXswrtdP1y1v0A37JO6NXACpAPzrnq4eNXbRnbhcdUoXW6PSHtLW45eKNvfFR/2TYf8+y1w0N9eQjEdzKo9A3FTjVtQ/wCfuT865fqlRaJnesyoSV5Q1Oy/smx/59ko/siw/wCfZK47+1tQ/wCfqT86Q6vqP/P3J+dH1Wr3H/aGG/k/I7WOxtLY7o4EU+uKztZ16GyhMULB5zwAO1crLqV9KNr3UhB6/NiqLcknk+561dPBtO83czqZmuVxpRsMkZpHZ2OWY5P1qMipalgs3mfOCF9+9dt7KyPI5W3qSWq+Vp8jngsMj8q6jwaCNKOe7muYv5V2C3j+pxXa+H7VrXTI1I5Iya4cVP8Ad27s9nLab9pfsjVdA8ZRhkEVweu6JLa3BuIFbYTnjtXfGmPGsikOMj0rghNwd0ezVpRqxtI8yh1F0GyRQ+PXrU4urR+Wh59xXX3fhuyuDnywCap/8IfaZNdSxfdHmyyy+0vwOeE9n/zxH/fNPE9n/wA8R/3zW9/wh9nR/wAIfZ1X1vy/ESyt/wA34GIJrP8A54j/AL5p4ntP+eVbH/CH2dH/AAh9nS+t+X4j/st/zL7jIMtr/wA8qaZrQf8ALEflWz/wh9nR/wAIfZ0fW/L8Q/st/wAy+4wzNaf88v0phns/+eP6Zrf/AOEPs6P+EPs6f1vy/EX9lv8Am/A577TaocrAM/SoZtQd/kiXaPaun/4Q+0q7a+G7K2ORGCfepeL8gjllnrL8DndC0SW6nW4nB2+9dyihFVVGAKEjWMBVG0CnVy1Kkpu7PTpUo0o8sT//2Q=="}
        		]
    	},
    	"customers": [
        	{
        		"eventId": "7578",
        		"stationId": "732335",
        		"submissionType": "LOOKUP",
        		"uidList": [
        			{
            			"name": "mtn",
            			"value": "2025492030"
            		}
        		],
        		"usersDataItems": [
        			{
            			"name": "MTN",
            			"value": "2025492030",
            			"timestamp": "2017-04-26 06:48:01"
            		}

        		],
        		"extraInfo": {
            		"DEVICE_TYPE": "samsung SM-G930V",
            		"DEVICE_NAME": "SAMSUNG-SM-G930V",
        			"DEVICE_MTN": "355301070620680",
            		"DEVICE_TIMEZONE": "Eastern Standard Time",
            		"ABOUT_APP": "App Version: 1.01.05\nApp Name: PHOTO\nPackage: com.bpcreates.geico.photo\nApi Endpoint: https://dev-api.bpcreates.com/api/\nTruly online (204 success):  no\n\nCampaign: GEICO Events 1Q17\nTeam: NASCAR\nEvent: NASCAR\nPending ServerCalls: 0Kb\nPending Failover Calls: 0Kb\nPending Cloud Files: 0\n\nSSID:\"benthere\""
        		},
        		"friends": []

        	}
        ]

      }