'use strict';
const fs = require('fs');
const path = require('path');
const async = require('async');
const Errorer = require('../../lib/classes/Errorer/Errorer.class');
const apiDataMapper = require('../../lib/classes/apiDataMapper/apiDataMapper.class');
module.exports = (req, res)=>{
    fs.readFile(path.join(__dirname, '../../temp_files/waiver_list.json'), 'utf8',(err, file)=>{
        if (err)
            return res.send({success:false, error:true, error_message:err.message});
        let P_file =  JSON.parse(file);
        apiDataMapper.sendUserWaiver(P_file, (err, result)=>{
            res.send({success:true, error:false});
        });
    });
};
