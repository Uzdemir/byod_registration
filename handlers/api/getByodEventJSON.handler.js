'use strict';
const fs = require('fs');
const sha512 = require('js-sha512');
const async = require('async');
const Errorer = require('../../lib/classes/Errorer/Errorer.class');
const apiDataMapper = require('../../lib/classes/apiDataMapper/apiDataMapper.class');
module.exports = (req, res)=>{
    let event_json = {};
    async.waterfall([
        (callback)=>{
            if (!('event_code' in req.query))
                return callback(new Errorer({message:'You need event code!'}), null);
            return callback(null, req.query.event_code);
        },
        (event_code, callback)=>{
            let json = null;
            if (!('event_jsons' in req.session))
                req.session.event_jsons = {};
            if ('byod_registration_token' in req.cookies){
                let token = req.cookies.byod_registration_token;
                if (token in req.session.event_jsons)
                    json = req.session.event_jsons[token];
            }
            return callback(null, event_code, json);
        },
        (event_code, json, callback)=>{
            if (json == null){
                let token = generateBYODRegistrationToken(event_code);
                apiDataMapper.getEventJSON(event_code, (err, json_obj)=>{
                    if (err)
                        return callback(new Errorer({message:'Error in getting event json from spock'}), null);
                    json = json_obj;
                    res.cookie('byod_registration_token', token, { maxAge: 3600000, httpOnly: false });
                    req.session.event_jsons[token] = json;
                    return callback(null, json);
                });
            } else
                return callback(null, json);
        }
    ], (err, json)=>{
        if (err)
            return res.send({error:true, success:false, error_message:err.message, event_json:null});
        try{
            let resultJSON = {};
            if ('reg_la' in req.cookies){
                console.log(req.cookies);
                for (let i in json.registration_applications){
                    if (json.registration_applications[i].language === req.cookies.reg_la)
                        resultJSON.registration_application = json.registration_applications[i];
                }
            }
            resultJSON.info = json.info;
            resultJSON.event_info = json.event_info;
            resultJSON.site_creative = json.site_creative;
            console.log(resultJSON);
            return res.send({error:false, success:true, error_message:null, event_json:resultJSON});
        } catch (err){
            return res.send({error:true, success:false, error_message:'Error in event json structure!', event_json:null});
        }
    });
};

function generateBYODRegistrationToken(eventCode) {
    let curTime = new Date().getTime();
    return sha512(`${curTime}${eventCode}`);
}