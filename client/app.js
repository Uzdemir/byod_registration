/**
 * Created by itersh on 11.05.17.
 */
const path = require('path')
    , express = require('express')
    , webpack = require('webpack')
    , app = express()
    , serveStatic = require('serve-static')

app.use("/", serveStatic(path.join(__dirname, './')))

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

const port = 3001

app.listen(port, (err) => {
    if (err) {
        console.log(err);
        return;
    }

    console.log('🚧  App is listening at http://%s:%s', port);
});
