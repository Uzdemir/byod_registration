'use strict';
class ApiDataMapper{
	constructor(connectionType){
		this.connectionType = connectionType;
		this.connection = null;
		if (connectionType == 'http')
			this.connection = require('http');
		else
			this.connection = require('https');
		this.async = require('async');
		this.apiOptions = require('./api.options');
		this.fs = require('fs');
		this.path = require('path');
	}

	getEventJSON(eventCode, callback){
        this.fs.readFile(this.path.join(__dirname, '../../../temp_files/event_json.json'), 'utf8',(err, file)=>{
        	if (err)
				return callback(err, null);
            let P_file =  JSON.parse(file);
            let error = null;
            return callback(error, P_file);
        });
	}

	sendUserWaiver(userWaiver, callback){
        let options = {
        	hostname: this.apiOptions.host,
        	port: this.apiOptions.port,
        	path: this.apiOptions.pathes.sendUserWaiver,
        	method: 'POST',
        	headers: {
        	'Content-Type': this.apiOptions.header.contentTypes.json
        	}
        };
        let reqData = '';
        const sendUserWaiver = this.connection.request(options, (res)=>{
        	res.on('data', (chunk) => {
        	reqData+=chunk;
        	});
        	res.on('end', () => {
        	reqData = JSON.parse(reqData);
        	console.log(reqData);
        	if(reqData.success){
        		callback(null, reqData);
        	} else{
        		//TODO::Handl error
        		callback(new Error('sendUserWaiver api error'));
        	}
        	});
        });

        sendUserWaiver.on('error', (err)=>{
        	callback(err);
        });
        userWaiver.auth = this.apiOptions.auth;
		let data = JSON.stringify(userWaiver);
        console.log(data);
        sendUserWaiver.write(data);
        sendUserWaiver.end();
	}
}

const apiDataMapper = new ApiDataMapper('http');
module.exports = apiDataMapper;