'use strict';
module.exports = {
	host:'localhost',
	port:'3000',
	auth:{
		agency: "bpc",
		apiKey: "e99a18c428cb38d5f260853678922e03"
	},
	pathes:{
        sendUserWaiver:'/api/process_activation'
	},
	header:{
		contentTypes:{
			json:'application/json'
		}
	}
};