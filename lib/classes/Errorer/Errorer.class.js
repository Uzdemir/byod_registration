'use strict';

module.exports = class Errorer extends Error{
  constructor({message}){
      super(message);
  }
};