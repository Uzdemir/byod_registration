'use strict';

class Logger{
    constructor({TEG}){
        this.modes = {
            LOG:'log',
            ERROR:'error',
            DIR:'dir'
        };
        this.TAG = TEG;
    }

    log({message, mode}){
        let newMess = '';
        if (typeof message === 'object'){
            try{
                newMess = JSON.parse(message);
            }catch (err){
                newMess = message;
            }
        } else {
            newMess = message;
        }
        if (mode in console){
            let consoleMode = console.log;
            return setImmediate(consoleMode, `==>${this.TAG} [${new Date()}] SAY: ${newMess}`);
        } else {
            return 'Incorrect console mode! ERROE in Logger';
        }
    }
}

module.exports = Logger;